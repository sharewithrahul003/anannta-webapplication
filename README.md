# anannta-webApplication

This application required the latest version of React JS

# Required Dependency


- It's required latest Node JS
- It's required latest React JS
- It's required latest NPM
- It's required latest version of Bootstrap
- It's required latest version of React Material

# Required Environment

- It's run on the npm server

# Steps to setup the application

1. Install the latest version on node js https://nodejs.org/en/
2. After installation of node js need to install the node package manager https://www.npmjs.com/get-npm.
3. Need to install the React JS which required the minimum version of 4.1 https://reactjs.org/
4. Need to install the Bootstarp 4 which required the minimum version of 4.1 https://getbootstrap.com/
5. Need to install the reactmaterial which required the minimum version of 3 https://material-ui.com/





