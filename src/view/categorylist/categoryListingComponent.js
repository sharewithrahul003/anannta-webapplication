import React, { Component } from 'react';
import './categoryListing.css';
import CardBoxComponent from '../../components/cardbox/cardBoxComponent';


import { StateContext } from '../../context/statecontext/statecontext';
import categoryData from '../../endPoint/productCategory';
// import { CheckBoxComboComponent } from '../../components/checkboxcombo/checkBoxComboComponent';

class CategoryListingComponent extends Component {
    static contextType = StateContext;
    componentDidMount() {
        this.setState({ categoryListingData: categoryData });
    }
    state = {
        categoryListingData: [],
    }
    // getselectedcartitme(item, index) {
    //     //updateCart({ type: 'updateCart', cartItem: item });
    //     console.log('inside categorylistingcomponent getting the cart item', item, index);
    // }

    render() {
        const [{ cartItem }, updateCart] = this.context;
        const getselectedcartitme = (item) => {
            updateCart({ type: 'updateCart', cartItem: item });
        }
        return (
            <div className="contentWrapper">
                <div className="rowWrapper">
                    <div className="row">
                        <div className="col-md-2">
                            {/* <CheckBoxComboComponent availableoption={this.state.availableOption} /> */}
                        </div>
                        <div className="col-md-10">
                            <div className="row">

                                {this.state.categoryListingData.map((cartItem, index) => (
                                    <div className="col-md-3 listingSpacing" key={index} >
                                        <CardBoxComponent getselectedcartitme={getselectedcartitme} cartitem={cartItem} />
                                    </div>
                                ))}

                                {/* <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div>
                                <div className="col-md-3 listingSpacing"><CardBoxComponent /></div> */}

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        )
    }
}


export default CategoryListingComponent;