import React, { Component } from 'react';
import Offer from '../../components/offer/offerComponent';
import OfferData from '../../endPoint/offerData';
import CartContentBox from '../../components/cartlistbox/carlistboxComponent';
import { Link } from "react-router-dom";


import './cart.css';


class CartComponent extends Component {

    componentDidMount() {
        this.setState({ offerData: OfferData });
    };
    state = {
        offerData: []
    };

    render() {
        return (
            <div className="contentWrapper">
                <div className="container">
                    <div className="row">
                        <div className="col-8">
                            <div>
                                {this.state.offerData && this.state.offerData.length > 0 ? <Offer OfferData={this.state.offerData} /> : ""}
                            </div>
                            <div className="mTop10">
                                <CartContentBox colfirst={this.state.col1} coltwo={this.state.col2} isButtonRequired={true} />
                            </div>

                        </div>
                        <div className="col-4">
                            <div className="offerWrapper">
                                <h6>Gift Wrap for Rs.25</h6>
                            </div>
                            <div className="offerWrapper mTop10 priceWrapperSticky">
                                <h6>Price Detail</h6>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Cart Total
                                </div>
                                    <div className="col text-right small-text">
                                        Rs. 1500
                                </div>
                                </div>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Discount
                                </div>
                                    <div className="col text-right small-text">
                                        - Rs. 500
                                </div>
                                </div>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Tax
                                </div>
                                    <div className="col text-right small-text">
                                        Rs. 50
                                </div>
                                </div>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Delivery Charges
                                </div>
                                    <div className="col text-right small-text">
                                        <span className="badge badge-secondary">Free</span>

                                    </div>
                                </div>
                                <hr />
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        <h6> Total</h6>
                                    </div>
                                    <div className="col text-right small-text">
                                        <h6>Rs.3000</h6>

                                    </div>
                                </div>
                                <button type="button" className="btn btn-primary width100"><Link to='/order'>Place Your Order</Link></button>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

export default CartComponent;