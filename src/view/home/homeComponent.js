import React, { Component } from "react";
import SliderComponent from "../../components/slider/sliderComponent";
import CardComponent from "../../components/card/cardComponent";
import FeatureBoxComponent from "../../components/featurebox/featureBoxComponent";
import GalleryComponent from "../../components/gallery/galleryComponent";

class HomeComponent extends Component {
    state = {
        featureBox: [
            {
                order: 1,
                heading: "Work ",
                headingCnt: "Essentials",
                bodyContent:
                    "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
                img: "../../assets/dresses.jpg"
            },
            {
                order: 2,
                heading: "Minimal ",
                headingCnt: "Perfection",
                bodyContent:
                    "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
                img: "'../../assets/tunics_1.jpg"
            }
        ],
        tileData: [
            {
                img: "/static/images/grid-list/breakfast.jpg",
                title: "Breakfast",
                author: "jill111",
                cols: 2,
                featured: true
            },
            {
                img: "/static/images/grid-list/burgers.jpg",
                title: "Tasty burger",
                author: "director90"
            },
            {
                img: "/static/images/grid-list/camera.jpg",
                title: "Camera",
                author: "Danson67"
            },
            {
                img: "/static/images/grid-list/morning.jpg",
                title: "Morning",
                author: "fancycrave1",
                featured: true
            },
            {
                img: "/static/images/grid-list/hats.jpg",
                title: "Hats",
                author: "Hans"
            },
            {
                img: "/static/images/grid-list/honey.jpg",
                title: "Honey",
                author: "fancycravel"
            },
            {
                img: "/static/images/grid-list/vegetables.jpg",
                title: "Vegetables",
                author: "jill111",
                cols: 2
            },
            {
                img: "/static/images/grid-list/plant.jpg",
                title: "Water plant",
                author: "BkrmadtyaKarki"
            },
            {
                img: "/static/images/grid-list/mushroom.jpg",
                title: "Mushrooms",
                author: "PublicDomainPictures"
            },
            {
                img: "/static/images/grid-list/olive.jpg",
                title: "Olive oil",
                author: "congerdesign"
            },
            {
                img: "/static/images/grid-list/star.jpg",
                title: "Sea star",
                cols: 2,
                author: "821292"
            },
            {
                img: "/static/images/grid-list/bike.jpg",
                title: "Bike",
                author: "danfador"
            }
        ]
    }
    render() {
        return (
            <div>
                <SliderComponent />
                {/* <CardComponent /> */}
                <div className="container marketing">
                    {this.state.featureBox.map((featureboxObj, index) => (
                        <React.Fragment key={index}>
                            <FeatureBoxComponent
                                featureItem={featureboxObj}
                            />
                            <hr className="featurette-divider" />
                        </React.Fragment>
                    ))}</div>
                <GalleryComponent galleryData={this.state.tileData} />

            </div>
        )
    }
}

export default HomeComponent;


