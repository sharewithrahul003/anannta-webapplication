import React, { Component } from 'react';
import StepperComponent from '../../components/stepper/stepperComponent';
class OrderComponent extends Component {
    state = {
        steps: [{ label: 'Address Info' }, { label: 'Payment Info' }, { label: 'Review order' }, { label: 'Thank you' }]
    }
    render() {
        return (
            <React.Fragment>
                <div className="contentWrapper">
                    <div className="container">
                        <StepperComponent steps={this.state.steps}></StepperComponent>
                    </div>

                </div>
            </React.Fragment>
        )
    }
}

export default OrderComponent;