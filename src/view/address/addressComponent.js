import React, { Component } from 'react';
import FormBuilderComponent from '../../components/formbuilder/formbuilderComponent';
import { makeStyles } from '@material-ui/core/styles';
import { red } from '@material-ui/core/colors';
import Icon from '@material-ui/core/Icon';
import DrawerComponent from '../../components/drawer/drawerComponent';
import index from 'react-swipeable-views';





class AddressComponent extends Component {

    componentDidMount() {
        this.setState({ addressInfo: JSON.parse(localStorage.getItem('addressObj')) });
    }

    state = {
        drawerObj: {
            title: 'Address',
            formObj: [{
                inputType: 'text',
                label: 'name',
                displayLabel: 'Name',
                name: '',
            },
            {
                inputType: 'number',
                label: 'contactNumber',
                displayLabel: 'Contact Number',
                name: ''
            }, {
                inputType: 'email',
                label: 'emailAddress',
                displayLabel: 'Email Address',
                name: ''
            },
            {
                inputType: 'text',
                label: 'addressLine1',
                displayLabel: 'Colony, Street, Village',
                name: ''
            },
            {
                inputType: 'text',
                label: 'addressLine2',
                displayLabel: 'Locality',
                name: ''
            },
            {
                inputType: 'text',
                label: 'landmarkPlace',
                displayLabel: 'Landmark Place',
                name: ''
            },
            {
                inputType: 'text',
                label: 'state',
                displayLabel: 'State',
                name: ''
            }, {
                inputType: 'number',
                label: 'pinCode',
                displayLabel: 'Pin Code',
                name: ''
            }]
        },
        addressInfo: {}


    }

    render() {
        const getFormObj = (formObj) => {
            let addressObj = {};
            formObj.forEach((e, index) => {
                addressObj[e.label] = e.name
            });
            this.setState({ addressInfo: addressObj });
            localStorage.setItem('addressObj', JSON.stringify(addressObj));
            console.log('inside address component', formObj);

        }
        return (
            <div  >

                <h6>{this.props.addresstype}      <DrawerComponent drawerObj={this.state.drawerObj} getFormObj={getFormObj} /></h6>
                <div className="row">
                    <div className="col-md-4">
                        <div className="card gutterSpace20">{this.state.addressInfo ? <address>
                            <strong>{this.state.addressInfo.name}</strong><br />
                            <a href={`mailto:${this.state.addressInfo.emailAddress}`}>{this.state.addressInfo.emailAddress}</a>
                            <br />{this.state.addressInfo.addressLine1}<br />
                            {this.state.addressInfo.addressLine2}<br />
                            {this.state.addressInfo.state}-{this.state.addressInfo.pinCode}<br />
                            <abbr title="Phone">P:</abbr>(123) {this.state.addressInfo.contactNumber}
                        </address> : ""}

                        </div>
                    </div>

                </div>



                {/* <FormBuilderComponent formobj={this.state.addressForm} /> */}
            </div>
        )
    }
}

export default AddressComponent;