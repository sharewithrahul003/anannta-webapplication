import React, { Component } from 'react';
import CartContentBox from '../../components/cartlistbox/carlistboxComponent';
import { Link } from "react-router-dom";


class ReviewComponent extends Component {
    state = {};
    render() {
        return (
            <div>
                <h6>Review Order</h6>
                <div className="row">
                    <div className="col-md-8">
                        <CartContentBox colfirst={this.state.col1} coltwo={this.state.col2} />

                    </div>
                    <div className="col-4">
                        <div>
                            <div className="offerWrapper">
                                <h6>Gift Wrap for Rs.25</h6>
                            </div>
                            <div className="offerWrapper mTop10 priceWrapperSticky">
                                <h6>Price Detail</h6>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Cart Total
                        </div>
                                    <div className="col text-right small-text">
                                        Rs. 1500
                        </div>
                                </div>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Discount
                        </div>
                                    <div className="col text-right small-text">
                                        - Rs. 500
                        </div>
                                </div>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Tax
                        </div>
                                    <div className="col text-right small-text">
                                        Rs. 50
                        </div>
                                </div>
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        Delivery Charges
                        </div>
                                    <div className="col text-right small-text">
                                        <span className="badge badge-secondary">Free</span>

                                    </div>
                                </div>
                                <hr />
                                <div className="row mrTB3">
                                    <div className="col small-text">
                                        <h6> Total</h6>
                                    </div>
                                    <div className="col text-right small-text">
                                        <h6>Rs.3000</h6>

                                    </div>
                                </div>
                            </div>

                            <button type="button" className="btn btn-primary width100"><Link to='/thankyou'>Place Your Order</Link></button>
                        </div>



                    </div>
                </div>
            </div>
        )
    }
}

export default ReviewComponent;