import React, { Component } from "react";
import PageNotFoundImg from '../../assets/404page.png'

class PageNotFoundComponent extends Component {

    render() {
        return (
            <div className="pageLayoutWrapper">
                <img src={PageNotFoundImg} alt="Page Not Found" width="100%" />
            </div>
        )
    }
}

export default PageNotFoundComponent;