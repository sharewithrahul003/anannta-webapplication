import React, { Component } from 'react';
import categoryData from '../../endPoint/productCategory';
import ToggleButtonComponent from '../../components/button/toggleButtonComponent';
import './productDetailComponent.css';


class ProductDetailComponent extends Component {
    getSelectedCart(size) {
        console.log('!!!!!!!!!!!!!!!!', size);
    }
    componentDidMount() {
        // console.log(categoryData);
        categoryData.forEach((item, index) => {

            if (item.productID == this.props.match.params.id) {
                console.log(item)
                this.setState({ productObj: item })
            }

        })
        // this.setState({categoryData:})
    }
    state = {
        productObj: {}
    }

    render() {
        return (
            <div className="contentWrapper">
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-4 productDetail_col1">
                        <div className="row">
                            <div className="col-md-6">
                                <img src={this.state.productObj.imageUrl} width="100%" />
                            </div>
                            <div className="col-md-6">
                                <img src={this.state.productObj.imageUrl} width="100%" />
                            </div>
                            </div>

                        </div>
                        <div className="col-md-8 productDetail_col2">
                            <h4 className="heading-medium mrNone">{this.state.productObj.productName}</h4>
                            <h5 className="heading-small mrNone">{this.state.productObj.discount ? <span>Rs {this.state.productObj.discountedPrice} <del>{this.state.productObj.actualPrice}</del><span className="mLR5 badge badge-success">{this.state.productObj.discount}</span></span> : <span>{this.state.productObj.actualPrice}</span>}</h5>
                            <div className="productInfoSizeCnt">Select Size:{this.state.productObj.availableSize ? <ToggleButtonComponent availablesize={this.state.productObj.availableSize} getselectedcart={this.getSelectedCart} /> : ''}</div>
                            <div>
                                <h5 className="heading-category mTop20">Product Description</h5>
                                <p className="lead lead-small">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.
                                </p>
                                <h5 className="heading-category mTop20">Material</h5>
                                <p className="lead lead-small">The model (height 5'8") is wearing a size S</p>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ProductDetailComponent;