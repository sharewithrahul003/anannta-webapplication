import React, { Component } from 'react';
// import Offer from '../../components/offer/offerComponent';
import OfferData from '../../endPoint/appliedOffer';
import Tabs from '../../components/tab/tabComponent';


class PaymentComponent extends Component {
    componentDidMount() {
        this.setState({ offerData: OfferData });
    };
    state = {
        offerData: [],

        "tabContent": [{
            "heading": "Credit / Debit Card",
            'form': [{
                inputType: 'text',
                label: 'Name',
                displayLabel: 'Name on Card',
                class: 'col-md-12'
            },
            {
                inputType: 'number',
                label: 'CreditCardNumber',
                displayLabel: 'Credit Card Number',
                class: 'col-md-12'
            },
            {
                inputType: 'number',
                label: 'ExpiryMonth',
                displayLabel: 'Expiry Month',
                class: 'col-md-4'
            },
            {
                inputType: 'number',
                label: 'Expiry Year',
                displayLabel: 'Expiry Year',
                class: 'col-md-4'
            },
            {
                inputType: 'password',
                label: 'CVV',
                displayLabel: 'CVV',
                class: 'col-md-4'
            }],
        },
        {
            "heading": "Net Banking",
            'form': [{
                inputType: 'text',
                label: 'Name',
                displayLabel: 'Name on Card'
            },
            {
                inputType: 'text',
                label: 'CreditCardNumber',
                displayLabel: 'Credit Card Number'
            },
            {
                inputType: 'text',
                label: 'CVV',
                displayLabel: 'CVV'
            },
            {
                inputType: 'text',
                label: 'Address Line 4',
                displayLabel: 'Address Line 4'
            },
            {
                inputType: 'text',
                label: 'Address Line 5',
                displayLabel: 'Address Line 5'
            }],
        },
        {
            "heading": "UPI",
            'form': [{
                inputType: 'text',
                label: 'Name',
                displayLabel: 'Name on Card'
            },
            {
                inputType: 'text',
                label: 'CreditCardNumber',
                displayLabel: 'Credit Card Number'
            },
            {
                inputType: 'text',
                label: 'CVV',
                displayLabel: 'CVV'
            },
            {
                inputType: 'text',
                label: 'Address Line 4',
                displayLabel: 'Address Line 4'
            },
            {
                inputType: 'text',
                label: 'Address Line 5',
                displayLabel: 'Address Line 5'
            }],
        },
        {
            "heading": "Wallets",
            'form': [{
                inputType: 'text',
                label: 'Name',
                displayLabel: 'Name on Card'
            },
            {
                inputType: 'text',
                label: 'CreditCardNumber',
                displayLabel: 'Credit Card Number'
            },
            {
                inputType: 'text',
                label: 'CVV',
                displayLabel: 'CVV'
            },
            {
                inputType: 'text',
                label: 'Address Line 4',
                displayLabel: 'Address Line 4'
            },
            {
                inputType: 'text',
                label: 'Address Line 5',
                displayLabel: 'Address Line 5'
            }],
        }]
    }
    render() {
        return (


            <div className="container">
                <div className="row">
                    <div className="col-7">
                        {/* <div>
                            {this.state.offerData && this.state.offerData.length > 0 ? <Offer OfferData={this.state.offerData} offerType="appliedOffer" /> : ""}
                        </div> */}
                        <div>


                            <h6>Select Payment Method</h6>
                            <Tabs tabData={this.state.tabContent} />
                            {/* <FormBuilderComponent formobj={this.state.paymentForm} /> */}

                        </div>

                    </div>
                    <div className="col-5">
                        <div className="offerWrapper">
                            <h6>Gift Wrap for Rs.25</h6>
                        </div>
                        <div className="offerWrapper mTop10 priceWrapperSticky">
                            <h6>Price Detail</h6>
                            <div className="row mrTB3">
                                <div className="col small-text">
                                    Cart Total
                            </div>
                                <div className="col text-right small-text">
                                    Rs. 1500
                            </div>
                            </div>
                            <div className="row mrTB3">
                                <div className="col small-text">
                                    Discount
                            </div>
                                <div className="col text-right small-text">
                                    - Rs. 500
                            </div>
                            </div>
                            <div className="row mrTB3">
                                <div className="col small-text">
                                    Tax
                            </div>
                                <div className="col text-right small-text">
                                    Rs. 50
                            </div>
                            </div>
                            <div className="row mrTB3">
                                <div className="col small-text">
                                    Delivery Charges
                            </div>
                                <div className="col text-right small-text">
                                    <span className="badge badge-secondary">Free</span>

                                </div>
                            </div>
                            <hr />
                            <div className="row mrTB3">
                                <div className="col small-text">
                                    <h6> Total</h6>
                                </div>
                                <div className="col text-right small-text">
                                    <h6>Rs.3000</h6>

                                </div>
                            </div>
                            <button type="button" className="btn btn-primary width100">Place Your Order</button>
                        </div>



                    </div>
                </div>
            </div>



        )
    }
}
export default PaymentComponent;