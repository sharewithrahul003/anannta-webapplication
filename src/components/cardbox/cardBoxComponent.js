import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import img1 from '../../assets/thumb1.jpg';
import Button from '@material-ui/core/Button';
import ToggleButtonComponent from '../button/toggleButtonComponent';
import './cardBox.css';
import DialogBox from '../dialog/dialogComponent';
import Dialog from '@material-ui/core/Dialog/Dialog';
import { Link } from "react-router-dom";




const useStyles = makeStyles({
    card: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
    margin: {
        margin: 3,
    }

});

function CardBoxComponent(props) {
    const classes = useStyles();
    const [isCartClicked, addItemToCart] = useState(true);
    const [availablesize, setAlignment] = React.useState(props.cartitem.availableSize);
    const getAddToItemAction = (action) => {
        addItemToCart(action);
        //console.log("Action TYpe", action);
    };

    const getSelectedCart = (item) => {
        addItemToCart(true);
        props.cartitem.selectedSize = item;
        props.getselectedcartitme(props.cartitem);
    }
    return (
        <div>


            <div className="productThumbWrapper">
                <div>
                    <div className="productInfoImageWrapper">
                        <Link to={`/productDetail/${props.cartitem.productID}`}><img src={props.cartitem.imageUrl} alt="" width="100%" /></Link>
                    </div>
                    <div className={isCartClicked ? 'productInfoWrapper' : 'productInfoWrapperActive'}>
                        <h6>{props.cartitem.productName}{!isCartClicked ? <span onClick={() => getAddToItemAction(true)} className="float-right">x</span> : ""}</h6>
                        <h6>Price: Rs
                            {props.cartitem.discountedPrice ? <span>{props.cartitem.discountedPrice} <del>{props.cartitem.actualPrice}</del></span> : <span>{props.cartitem.actualPrice}</span>}
                            {props.cartitem.discount ? <span className="mLR5 badge badge-success">{props.cartitem.discount}</span> : ''}
                        </h6>
                        <p className="lead productInfoLeadTxt"><strong>Size </strong>:
                        {availablesize.length > 0 ? availablesize.map((item, index) => (
                                <span key={index}>{item.value},</span>
                            )) : ''}
                        </p>


                        <div className="productInfoSlider">
                            <div>
                                <Button variant="outlined" size="small" color="primary" className={classes.margin} onClick={() => addItemToCart(false)}>
                                    Add to Cart
        </Button>
                                <DialogBox type="PhotoGallery" size="md" buttonsize="small" buttontitle="Quick View" />

                                <div className="productInfoSizeCnt">Select Size:{availablesize ? <ToggleButtonComponent availablesize={availablesize} getselectedcart={getSelectedCart} /> : ''}</div>
                            </div>

                        </div>
                    </div>

                </div>




            </div>

        </div>
    )

}

export default CardBoxComponent;