import React, { Component } from "react";
import { Link } from "react-router-dom";
import './gallery.css';
import img1 from '../../assets/img-1.png';
import img2 from '../../assets/img-2.png';
import img3 from '../../assets/img-3.png';
import img4 from '../../assets/img-4.png';
import img5 from '../../assets/img-5.png';
import img6 from '../../assets/img-6.png';
import img7 from '../../assets/andrise2_1.jpg';

import Skeleton from '@material-ui/lab/Skeleton';


// import DialogBox from '../dialog/dialogComponent'


// Remote data from local file
import galleryLoader from '../../endPoint/galleryData.js'





class GalleryComponent extends Component {

  // Need to un-comment the code when we are passing the props from parent
  // constructor(props) {
  //   super(props);
  // }

  componentDidMount() {
    // fetch('./endPoint/galleryData.json')
    //   .then(res => res.json())
    //   .then((data) => {
    //     this.setState({ galleryData: __Gallerydata });
    //   })
    //   .catch(console.log);

    this.setState({ galleryData: galleryLoader() });
  }
  state = {
    galleryData: [],
  };

  getSelectedObj(item) {
    const path = '/category';
  }


  renderSwitchImg(imgID) {
    switch (imgID.src) {

      case 'img-1':
        imgID.imageURL = img1;
        return <img className="img-responsive" src={img1} alt="" />
        break;

      case 'img-2':
        imgID.imageURL = img2;
        return <img className="img-responsive" src={img2} alt="" />
        break;

      case 'img-3':
        imgID.imageURL = img3;
        return <img className="img-responsive" src={img3} alt="" />
        break;

      case 'img-4':
        imgID.imageURL = img4;
        return <img className="img-responsive" src={img4} alt="" />
        break;
      case 'img-5':
        imgID.imageURL = img5;
        return <img className="img-responsive" src={img5} alt="" />
        break;

      case 'img-6':
        imgID.imageURL = img6;
        return <img className="img-responsive" src={img6} alt="" />
        break;


      case 'img-7':
        imgID.imageURL = img7;
        return <img className="img-responsive" src={img7} alt="" />
        break;
    }
  }

  render() {
    return (
      <div>
        <h2 className="featurette-heading text-center mr-t45">
          FALL WINTER'
 <span className="text-muted">
            19
            </span>
        </h2>
        <p className="lead text-center">Sed fermentum sem sit amet mauris luctus, id mollis libero convallis.<br />Phasellus ac nisl sem. Donec sit amet nunc ligula. Donec sodales interdum tortor ac pretium.</p>
        <div className="Imgrow" >
          {this.state.galleryData ?
            this.state.galleryData.map((data, index) => (
              <div className="Imgcolumn" key={index}>
                {data.galleryData && data.galleryData.length > 0 ? data.galleryData.map((childData, index) => (
                  <div className="hovereffect" key={index}>
                    {childData.src ? this.renderSwitchImg(childData) : <Skeleton variant="rect" width={210} height={118} />}                    <div className="overlay">
                      <h2>{childData.title}</h2>
                      <button onClick={() => this.getSelectedObj(data)}><Link to="category">View Collection</Link></button>


                      {/* <DialogBox dialogObj={childData} type="PhotoGallery" size="md" /> */}

                    </div>
                  </div>
                )) : "subchild not available"}

              </div>
            ))
            : <div>GalleryData Not available</div>}
        </div>
      </div>
    );
  }
}

export default GalleryComponent;
