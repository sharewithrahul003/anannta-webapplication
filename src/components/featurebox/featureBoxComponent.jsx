import React, { Component } from "react";
import "./featureBox.css";
// import Skeleton from '@material-ui/lab/Skeleton';

class FeatureBoxComponent extends Component {
  state = {};
  render() {
    console.log("this", this.props);
    return (
      <div className="row">
        <div
          className={
            this.props.featureItem.order === 1
              ? "col-md-7"
              : "col-md-7 order-md-2"
          }
        >
          <h2 className="featurette-heading">
            {this.props.featureItem.heading}
            <span className="text-muted">
              {this.props.featureItem.headingCnt}
            </span>
          </h2>
          <p className="lead">{this.props.featureItem.bodyContent}</p>
        </div>
        <div className="col-md-5">
          {/* <img src={this.props.featureItem.img} alt="" /> */}
          {/* <Skeleton variant="rect" width={210} height={118} /> */}

          <svg
            className="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto"
            width="500"
            height="500"
            xmlns="http://www.w3.org/2000/svg"
            preserveAspectRatio="xMidYMid slice"
            focusable="false"
            role="img"
            aria-label="Placeholder: 500x500"
          >
            <title>Placeholder</title>
            <rect width="100%" height="100%" fill="#eee" />
            <text x="50%" y="50%" fill="#aaa" dy=".3em" />
          </svg>
        </div>
      </div>
    );
  }
}

export default FeatureBoxComponent;
