import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import { useStateValue } from '../../context/statecontext/statecontext';


const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(1),
    }
}));
export default function CartContentBox(props) {
    const classes = useStyles();
    console.log('----------', props)


    const [{ cartItem }, removeItem] = useStateValue();
    return (
        <div>
            {/* {JSON.stringify(cartItem.itemList)}- */}
            {cartItem.itemList.map((item, index) => (
                <div className="card mb-3" key={index}>
                    <div className="row no-gutters">
                        <div className={props.col1 ? props.col1 : 'col-md-2'}>
                            <img src={item.imageUrl} className="card-img" alt="" />
                        </div>
                        <div className={props.col2 ? props.col2 : 'col-md-9'}>
                            <div className="card-body">
                                <h5 className="card-title">{item.productName}{item.discountedPrice ? <span className="mLR5 badge badge-success">{item.discount}</span> : ''}<span className="float-right">Rs. {item.discountedPrice ? item.discountedPrice : item.actualPrice}</span></h5>
                                <h6>Selected Size:{item.selectedSize.value}</h6>
                                {/* <p className="small-text">Selected Size:{JSON.stringyfy(item.selectedSize}</p> */}
                                {props.isButtonRequired ? <Button variant="outlined" size="small" color="primary" className={classes.margin} onClick={() => removeItem({ type: 'removeCart', cartItem: item, index: index })}>
                                    Remove</Button> : ''}
                                <div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ))}

        </div>
    );
}