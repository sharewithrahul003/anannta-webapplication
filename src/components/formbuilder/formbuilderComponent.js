import React from 'react';
import OutlinedTextFields from '../../components/textfield/textfieldComponent';
import ButtonComponent from '../../components/button/buttonComponent';

function FormBuilderComponent(props) {
    function getFormData() {
        props.getFormData(FormBuilderObj);
    }
    var FormBuilderObj = ([...props.formobj]);

    function getFieldObj(obj, index) {
        FormBuilderObj[index] = obj;
    }
    return (
        <div>
            <form noValidate autoComplete="off" >
                <div className="row">
                    {props.formobj.map((formItem, index) => (
                        <div className={formItem.class ? formItem.class : 'col-md-12'} key={index}>
                            <OutlinedTextFields formitem={formItem} itemIndex={index} getFieldObj={getFieldObj} />
                        </div>

                    ))}
                    <div className="col-md-12" onClick={() => getFormData()} >
                        <ButtonComponent color="primary" label="Submit Details" />

                        {/* <button onClick={() => getFormData()}>Submit Info</button> */}
                    </div>

                </div>
            </form>
        </div>
    )
}

export default FormBuilderComponent;