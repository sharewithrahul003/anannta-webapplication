import React from 'react';
import Grid from '@material-ui/core/Grid';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';

export default function ToggleButtonSizes(props) {
    const [alignment, setAlignment] = React.useState('left');

    const handleChange = (event, newAlignment) => {
        setAlignment(newAlignment);
    };

    const getSelectedVal = (selectedItem) => {
        props.getselectedcart(selectedItem);
        console.log('Size button triggered', selectedItem);
    }
    const children = [
        <ToggleButton key={1} value="XS" >
            XS
        </ToggleButton>,
        <ToggleButton key={2} value="SM">
            SM
        </ToggleButton>,
        <ToggleButton key={3} value="LG">
            Lg       </ToggleButton>,
        <ToggleButton key={4} value="Med">
            Med
        </ToggleButton>,
        <ToggleButton key={5} value="XL">
            XL
        </ToggleButton>,
        <ToggleButton key={6} value="XXL">
            XXL
      </ToggleButton>,
    ];
    return (
        <Grid container spacing={2} direction="column" alignItems="left">
            <Grid item>
                <ToggleButtonGroup size="small" value={alignment} exclusive onChange={handleChange} >

                    {props.availablesize.length > 0 ? props.availablesize.map((item, index) => (
                        <ToggleButton key={index} value={item.value} onClick={() => getSelectedVal(item)}>
                            {item.value}
                        </ToggleButton>
                    )) : ''}
                </ToggleButtonGroup>
            </Grid>
        </Grid>
    );
}