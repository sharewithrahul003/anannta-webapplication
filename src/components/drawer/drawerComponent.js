import React from 'react';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { Link } from "react-router-dom";
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import MailIcon from '@material-ui/icons/Mail';
import IconButton from '@material-ui/core/IconButton';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import { useStateValue } from '../../context/statecontext/statecontext';
import CartContentBox from '../../components/cartlistbox/carlistboxComponent';



import Badge from '@material-ui/core/Badge';
import FormbuilderComponent from '../formbuilder/formbuilderComponent';
const useStyles = makeStyles({
    list: {
        width: 430,
    },
    fullList: {
        width: 'auto',
    },
    cartContentWrapper: {
        padding: 20,
        display: 'flex',
        flexDirection: 'column',
    },
    title: {
        position: 'static',
        top: 90,
    }
});


const StyledBadge1 = withStyles(theme => ({
    badge: {
        right: -3,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}))(Badge);


export default function SwipeableTemporaryDrawer(props) {
    console.log('Address info =====', props.drawerObj.formObj)
    const [{ cartItem }] = useStateValue();

    const classes = useStyles();
    const [state, setState] = React.useState({
        top: false,
        left: false,
        bottom: false,
        right: false,
    });

    const getControl = (itemType) => {
        switch (itemType.title) {
            case 'My Cart': return (<CartContentBox col1="col-md-3" col2="col-md-9" isButtonRequired={location.pathname.indexOf('order') === -1 ? true : false} />)
                break;
            case 'Address': return (<FormbuilderComponent formobj={props.drawerObj.formObj} getFormData={getFormData} />)
                break;
        }


    }

    const toggleDrawer = (side, open) => event => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        setState({ ...state, [side]: open });
    };
    const getFormData = (obj) => {
        setState({ ...state, ['right']: false });
        props.getFormObj(obj);
        console.log('Inside drawer component', obj)
    }

    const sideList = side => (
        <div
            className={classes.list}
            role="presentation"

        >
            <div className={classes.cartContentWrapper}>
                <h4 className={classes.title}>{props.drawerObj.title ? props.drawerObj.title : 'Cart Info'}</h4>

                {getControl(props.drawerObj)}


                {location.pathname.indexOf('order') === -1 ? <div><Link to="order" onClick={toggleDrawer('right', false)}>Place your order</Link></div> : ""}

            </div>


        </div>
    );

    const fullList = side => (
        <div
            className={classes.fullList}
            role="presentation"
            onClick={toggleDrawer(side, false)}
            onKeyDown={toggleDrawer(side, false)}
        >
            <List>
                {['Inbox1', 'Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            <List>
                {['All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </div>
    );

    return (
        <div>
            <IconButton aria-label="cart" onClick={toggleDrawer('right', true)}>
                {props.drawerObj.title && props.drawerObj.title.toUpperCase() === 'MY CART' ? <StyledBadge1 badgeContent={cartItem.items} color="primary">
                    <ShoppingCartIcon />
                </StyledBadge1> : "Add Address"}

            </IconButton>
            {/* <Button onClick={toggleDrawer('left', true)}>Open Left</Button>
            <Button onClick={toggleDrawer('right', true)}>Open Right</Button>
            <Button onClick={toggleDrawer('top', true)}>Open Top</Button>
            <Button onClick={toggleDrawer('bottom', true)}>Open Bottom</Button> */}
            <SwipeableDrawer
                open={state.left}
                onClose={toggleDrawer('left', false)}
                onOpen={toggleDrawer('left', true)}
            >
                {sideList('left')}
            </SwipeableDrawer>
            <SwipeableDrawer
                anchor="top"
                open={state.top}
                onClose={toggleDrawer('top', false)}
                onOpen={toggleDrawer('top', true)}
            >
                {fullList('top')}
            </SwipeableDrawer>
            <SwipeableDrawer
                anchor="bottom"
                open={state.bottom}
                onClose={toggleDrawer('bottom', false)}
                onOpen={toggleDrawer('bottom', true)}
            >
                {fullList('bottom')}
            </SwipeableDrawer>
            <SwipeableDrawer
                anchor="right"
                open={state.right}
                onClose={toggleDrawer('right', false)}
                onOpen={toggleDrawer('right', true)}
            >
                {sideList('right')}
            </SwipeableDrawer>
        </div>
    );
}
