import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import HomeComponent from "../../view/home/homeComponent";
import CartComponent from "../../view/cart/cartComponent";
import PageNotfound from "../../view/notfound/notfoundComponent";
import OrderComponent from "../../view/order/orderComponent";
import CategoryListingComponent from "../../view/categorylist/categoryListingComponent";
import ProductDetailComponent from "../../view/productdetail/productDetailComponent";
export default () =>
    <Switch>
        <Redirect exact from='/' to='/home' />
        <Route exact path="/home" component={HomeComponent} />
        <Route exact path="/cart" component={CartComponent} />
        <Route exact path="/category" component={CategoryListingComponent} />
        <Route exact path="/order" component={OrderComponent} />
        <Route exact path="/productDetail/:id" component={ProductDetailComponent} />
        <Route exact component={PageNotfound} />
    </Switch>

