import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';

import Grid from '@material-ui/core/Grid';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Tabs from '../tab/tabComponent';

import Rating from '../rating/ratingComponent';
import { Redirect } from 'react-router-dom';

import { useStateValue } from '../../context/statecontext/statecontext';

import './dialog.css';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});



export default function AlertDialogSlide(props) {
    const [{ cartItem }, addItemToCart] = useStateValue();
    const [open, setOpen] = React.useState(false);
    const [fullWidth, setFullWidth] = React.useState(true);
    const [maxWidth, setMaxWidth] = React.useState(props.size ? props.size : 'sm');

    function handleClickOpen() {
        setOpen(true);
    }

    function handleClose(item) {
        setOpen(false);
        addItemToCart({ type: 'updateCart', cartItem: item });
        return <Redirect to='/cart' />;
    }

    return (


        <React.Fragment>
            <Button variant="outlined" color="primary" size={props.buttonsize} onClick={handleClickOpen}>
                {props.buttontitle}      </Button>
            <Dialog
                open={open}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleClose}
                fullWidth={fullWidth}
                maxWidth={maxWidth}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
            >dkfdkf
                {/* {(props.type && props.type.toUpperCase() !== "PHOTOGALLERY") ? <DialogTitle id="alert-dialog-slide-title">{props.dialogObj.Heading}</DialogTitle> : <DialogTitle id="alert-dialog-slide-title"></DialogTitle>}
                <div className="container">
                    <div className="row">
                        <div className="col" >
                            <div className="dialogImageWrapper" style={{ background: `url(${props.dialogObj.imageURL})` }} >
                            </div>
                        </div>
                        <div className="col">
                            <DialogTitle id="alert-dialog-slide-title" className="fixedHgt90">{props.dialogObj && props.dialogObj.title ? props.dialogObj.title : ""} <Rating /></DialogTitle>

                            <DialogContent>
                                <DialogContentText id="alert-dialog-slide-description">

                                    {props.dialogObj.description}
                                </DialogContentText>
                            </DialogContent>
                            <div className="sizeChart">
                                <h6>Available Sizes</h6>
                                <Grid item>
                                    <ButtonGroup size="small" aria-label="small outlined button group">
                                        <Button>S</Button>
                                        <Button>M</Button>
                                        <Button>XL</Button>
                                        <Button>XXL</Button>
                                    </ButtonGroup>
                                </Grid>
                            </div>
                            <div className="container productDetailWrapper">
                                {props.dialogObj.tabContent.length > 0 ? <Tabs tabData={props.dialogObj.tabContent} /> : ''}

                            </div>
                            <div>
                            </div>
                        </div>
                    </div>
                </div> */}
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={() => handleClose(props.dialogObj)} color="primary">
                        Add to Cart
          </Button>
                </DialogActions>
            </Dialog>
        </React.Fragment >
    );
}


