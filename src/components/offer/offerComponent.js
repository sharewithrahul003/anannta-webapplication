import React from 'react';
import './offer.css';


const OfferComponent = (props) => {
    return (
        <div>
            <div className="offerWrapper">
                <h6 className="heading-success">Offer Details</h6>
                <ul className="offerListing">
                    {props.OfferData.map((item, index) => (
                        <li key={index}>{item.value}</li>
                    ))}
                </ul>
            </div>
            {props.offerType && props.offerType.toUpperCase() !== "APPLIEDOFFER" ? <div className="offerWrapper mTop10">
                <h6> Free Delivery on this order.</h6>
            </div> : ""}
        </div>
    )
};

export default OfferComponent;

