import React, { Component } from "react";
import { Link } from "react-router-dom";

import "./nav.css";
import logo from "../../logo.svg";
import DrawerComponent from '../drawer/drawerComponent'

// Context api is implemented on component level we can remove this once implemented on the application level
// import { ThemeContext } from '../../context/themcontext/theme-context';

import { StateContext } from '../../context/statecontext/statecontext';



class Navigation extends Component {
  constructor(props) {
    super(props);
  }
  state={
    drawerObj: {
      title: 'My Cart'
  }
  }
  static contextType = StateContext;

  // When the user scrolls down 50px from the top of the document, resize the header's font size
  triggerChildAlert() {
    this.triggerChild();
  }

  render() {
    const [{ theme }, dispatch] = this.context;

    // let theme = this.context;
    window.onscroll = function () {
      scrollFunction();
    };

    const scrollFunction = () => {
      if ((document.body.scrollTop > 70 || document.documentElement.scrollTop > 70) || (location.pathname.indexOf('home') === -1)) {
        document.getElementById("navigationBar").classList.add("whiteHeader");
      } else {
        document.getElementById("navigationBar").classList.remove("whiteHeader");
      }
    }


    // console.log("this", this.props.navData);
    return (
      <nav className="navbar navbar-expand-md navbar-dark fixed-top" id="navigationBar">

        {/* <a className="navbar-brand" style={{ backgroundColor: theme.background }}> */}
        <a className="navbar-brand" >
          <img src={logo} className="App-logo" alt="logo" />
          {/* <img src={logo} className="App-logo" alt="logo" style={{ backgroundColor: theme.primary }} /> */}
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            {this.props.navData.map((nav, index) => (
              <li className="nav-item" key={index}>
                <Link className="nav-link" to={nav.url}>{nav.label}</Link>

                {/* <a className="nav-link" href="#">
                  {nav.label} <span className="sr-only">(current)</span>
                </a> */}
              </li>
            ))}
          </ul>

          <form className="form-inline mt-2 mt-md-0">
            <input
              className="form-control mr-sm-2"
              type="text"
              placeholder="Search"
              aria-label="Search"
            />
            {/* <button
              className="btn btn-outline-success my-2 my-sm-0"
              type="submit"
            >
              Search
            </button> */}
          </form>
          <div>
          <DrawerComponent drawerObj={this.state.drawerObj} />
                    </div>
        </div>

        {/* <button
          onClick={() => dispatch({
            type: 'changeTheme',
            newTheme: { primary: 'blue' }
          })}
        >
          Make me blue!
      </button>
        <button
          onClick={() => dispatch({
            type: 'changeTheme',
            newTheme: { primary: 'green' }
          })}
        >
          Make me Green!
      </button> */}
      </nav>
    );
  }
}
// Navigation.contextType = ThemeContext;
export default Navigation;
