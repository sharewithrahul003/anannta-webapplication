import React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import index from 'react-swipeable-views';
import Paper from '@material-ui/core/Paper';
import FormBuilderComponent from '../../components/formbuilder/formbuilderComponent';



function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    border: 'solid 1px #eaeaec',
    '& .MuiPaper-root': {
      boxShadow: 'none',
      borderBottom: 'solid 1px #eaeaec',
    }
  },
  tabroot: {
    flexGrow: 1,
  },
}));

export default function FullWidthTabs(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  function handleChange(event, newValue) {
    setValue(newValue);
  }

  function handleChangeIndex(index) {
    setValue(index);
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Paper className={classes.tabroot}>

          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
            scrollButtons="auto"
            variant="scrollable"
            aria-label="full width tabs example"
          >
            {props.tabData.map((obj, index) =>
              <Tab label={obj.heading} {...a11yProps(0) } key={index} />
            )}
          </Tabs>
        </Paper>

      </AppBar>
      {props.tabData.map((obj, index) => (
        <TabPanel value={value} index={index} key={index}>
          {obj.form ? <FormBuilderComponent formobj={obj.form} /> : <div dangerouslySetInnerHTML={{ __html: obj.content }}></div>}
        </TabPanel>
      ))}
    </div>
  );
}