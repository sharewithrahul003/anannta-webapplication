import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';



const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '100%',
    },
    dense: {
        marginTop: theme.spacing(2),
    },

}));


function OutlinedTextFields(props) {
    const classes = useStyles();
    const [values, setValues] = React.useState(props.formitem);

    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
    };
    const getInputVal = (value) => {
        props.getFieldObj(value, props.itemIndex);

    }
    return (
        <div>
            <TextField
                id="outlined-dense"
                label={values.displayLabel}
                className={clsx(classes.textField, classes.dense)}
                margin="dense"
                type={values.inputType}
                onChange={handleChange('name')}
                value={values.name}
                variant="outlined"
                onBlur={getInputVal(values)}
            />
        </div>
    )
}

export default OutlinedTextFields;