import React, { Component } from "react";
import "./slider.css";
import galleryImage from '../../assets/galleryImg.png';

class SliderComponent extends Component {
  state = {};
  render() {
    return (
      <div className="jumbotron text-center skyBlueBG ">
        {/* <img src={galleryImage} alt="" /> */}
        
        <div className="container bannerText">
          <h1 className="jumbotron-heading whiteColor">Banner Heading</h1>
          <p className="lead text-muted whiteColor">
            Something short and leading about the collection below—its contents,
            the creator, etc. Make it short and sweet, but not too short so
            folks don’t simply skip over it entirely.
          </p>
        </div>
      </div>
    );
  }
}

export default SliderComponent;
