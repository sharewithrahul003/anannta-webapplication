const data =
    [
        {
            "value": "10% Discount on Beauty & Personal Care Products using SBI Credit Cards on minimum spend of Rs 2000 . TCA"
        },
        
        {
            "value": "10% Cashback up to Rs 200 on PayZapp. TCA"
        },
        {
            "value": "Flat Rs 200 Cashback on Airtel Payments Bank. TCA"
        }
    ];

export default data;