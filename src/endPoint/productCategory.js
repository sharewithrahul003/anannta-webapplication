import img1 from '../assets/thumb1.jpg';
import img2 from '../assets/thumb2.jpg';
import img3 from '../assets/thumb3.jpg';
import img4 from '../assets/thumb4.jpg';
import img5 from '../assets/thumb5.jpg';
import img6 from '../assets/thumb6.jpg';

const categoryData = [{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img1,
    'category': "Ethenic",
    'productID': 1,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img2,
    'category': "Ethenic",
    'productID': 2,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img3,
    'category': "Ethenic",
    'productID': 3,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Printed Dress",
    'actualPrice': 3000,
    'imageUrl': img4,
    'category': "Ethenic",
    'productID': 4,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Maxi Dress",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img5,
    'category': "Ethenic",
    'productID': 5,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img6,
    'category': "Ethenic",
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
}, {
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img1,
    'category': "Ethenic",
    'productID': 6,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img2,
    'category': "Ethenic",
    'productID': 7,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img3,
    'category': "Ethenic",
    'productID': 8,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Printed Dress",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img4,
    'category': "Ethenic",
    'productID': 9,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Maxi Dress",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img5,
    'category': "Ethenic",
    'productID': 10,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
},
{
    'productName': "Varanga",
    'actualPrice': 3000,
    'discountedPrice': 2400,
    'discount': '20%',
    'imageUrl': img6,
    'category': "Ethenic",
    'productID': 11,
    'availableSize': [{ value: 'XS' }, { value: 'SM' }, { value: 'LG' }, { value: 'XL' }, { value: 'XXL' }]
}]

export default categoryData;