// images.js
const data =
  [
    {
      "galleryData": [
        {
          "id": 1,
          "src": "img-1",
          "title": "Collection 1",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]

        },
        {
          "id": 2,
          "src": "img-7",
          "title": "Collection 2",
          "description": "Sed quis tincidunt odio, at scelerisque odio. Nam imperdiet nisi eu sagittis mattis.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 3,
          "src": "img-1",
          "title": "Collection 3",
          "description": "Cras lobortis risus purus, at tristiquesed ultricies. Curabitur blandit elementum elit.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        }
      ]
    },
    {
      "galleryData": [
        {
          "id": 1,
          "src": "img-7",
          "title": "Collection 4",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 2,
          "src": "img-4",
          "title": "Collection 5",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 2,
          "src": "img-6",
          "title": "Collection 6",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        }
      ]
    },
    {
      "galleryData": [
        {
          "id": 1,
          "src": "img-5",
          "title": "Collection 7",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 2,
          "src": "img-6",
          "title": "Collection 8",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 2,
          "src": "img-7",
          "title": "Collection 9",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        }
      ]
    },
    {
      "galleryData": [
        {
          "id": 1,
          "src": "img-7",
          "title": "Collection 10",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 2,
          "src": "img-4",
          "title": "Collection 11",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        },
        {
          "id": 2,
          "src": "img-4",
          "title": "Collection 12",
          "description": "Cras lobortis risus purus, at tristique risus vestibulum sed. Sed elementum at sem sed ultricies.",
          "tabContent": [{
            "heading": "Product Description",
            "content": "<h6>Size & Fit</h6><p>The model (height 6') is wearing a size M</p><h6>Material & Care</h6><p>100% cottonMachine-wash</p>"
          },
          {
            "heading": "Vendor Details",
            "content": "<h6>Vendor Details:</h6><p>Himanshu Kumar</p><p>Sumit Sharma</p>"
          }]
        }
      ]
    }
  ];

function galleryLoader() {
  return data;
}

export default galleryLoader;