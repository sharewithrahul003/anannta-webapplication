const data =
    [
        {
            "productName": "Zima Leto Women Mustard Self Design Sheath Dress",
            "price": 3000,
            "category": "Ethenic",
            "quantity": 2,
            "availableSizes": ["S", "L", "XL", "XXL"],
            "selectedSize": "XL"
        },

        {
            "productName": "INVICTUS Men Navy Checked Slim Fit Single-Breasted Casual Blazer",
            "price": 3200,
            "category": "Western",
            "quantity": 2,
            "availableSizes": ["S", "L", "XL", "XXL"],
            "selectedSize": "XL"
        },
        {
            "productName": "Women Mustard Self Design Sheath Dress",
            "price": 2000,
            "category": "Indo-Western",
            "quantity": 2,
            "availableSizes": ["S", "L", "XL", "XXL"],
            "selectedSize": "XL"
        },
        {
            "productName": "Zima Leto Women Mustard Self Design Sheath Dress",
            "price": 3000,
            "category": "Ethenic",
            "quantity": 2,
            "availableSizes": ["S", "L", "XL", "XXL"],
            "selectedSize": "XL"
        },

        {
            "productName": "INVICTUS Men Navy Checked Slim Fit Single-Breasted Casual Blazer",
            "price": 3200,
            "category": "Western",
            "quantity": 2,
            "availableSizes": ["S", "L", "XL", "XXL"],
            "selectedSize": "XL"
        },
        {
            "productName": "Women Mustard Self Design Sheath Dress",
            "price": 2000,
            "category": "Indo-Western",
            "quantity": 2,
            "availableSizes": ["S", "L", "XL", "XXL"],
            "selectedSize": "XL"
        }
    ];

export default data;