import React from 'react';


export const CartReducer = (state, action) => {
    switch (action.type) {
        case 'changeTheme':
            return {
                ...state,
                theme: action.newTheme
            };
        case 'changeTheme':
            return {
                ...state,
                cartItem: action.cart
            }
        default:
            return state;
    }
}

export default CartReducer;