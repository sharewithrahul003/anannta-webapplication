import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
import Navigation from "./components/nav/navigationComponent";
import Route from './components/route/routeComponent';



// Implementing the application level context
import { StateProvider } from './context/statecontext/statecontext';
import CartReducer from './reducers/cart-reducer'

// import Button from "@material-ui/core/Button";
// import Checkbox from "@material-ui/core/Checkbox";



class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      navigationData: [
        { label: "Home", id: 1, url: "/home" },
        { label: "About", id: 2, url: "/about" },
        { label: "Contact", id: 3, url: "/contact" },
        { label: "My Cart", id: 4, url: "/cart" },
        { label: "Category List", id: 4, url: "/category" }
      ],
      checkedA: true,
      featureBox: [
        {
          order: 1,
          heading: "Work ",
          headingCnt: "Essentials",
          bodyContent:
            "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
          img: "../../assets/dresses.jpg"
        },
        {
          order: 2,
          heading: "Minimal ",
          headingCnt: "Perfection",
          bodyContent:
            "Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.",
          img: "'../../assets/tunics_1.jpg"
        }
      ],
      tileData: [
        {
          img: "/static/images/grid-list/breakfast.jpg",
          title: "Breakfast",
          author: "jill111",
          cols: 2,
          featured: true
        },
        {
          img: "/static/images/grid-list/burgers.jpg",
          title: "Tasty burger",
          author: "director90"
        },
        {
          img: "/static/images/grid-list/camera.jpg",
          title: "Camera",
          author: "Danson67"
        },
        {
          img: "/static/images/grid-list/morning.jpg",
          title: "Morning",
          author: "fancycrave1",
          featured: true
        },
        {
          img: "/static/images/grid-list/hats.jpg",
          title: "Hats",
          author: "Hans"
        },
        {
          img: "/static/images/grid-list/honey.jpg",
          title: "Honey",
          author: "fancycravel"
        },
        {
          img: "/static/images/grid-list/vegetables.jpg",
          title: "Vegetables",
          author: "jill111",
          cols: 2
        },
        {
          img: "/static/images/grid-list/plant.jpg",
          title: "Water plant",
          author: "BkrmadtyaKarki"
        },
        {
          img: "/static/images/grid-list/mushroom.jpg",
          title: "Mushrooms",
          author: "PublicDomainPictures"
        },
        {
          img: "/static/images/grid-list/olive.jpg",
          title: "Olive oil",
          author: "congerdesign"
        },
        {
          img: "/static/images/grid-list/star.jpg",
          title: "Sea star",
          cols: 2,
          author: "821292"
        },
        {
          img: "/static/images/grid-list/bike.jpg",
          title: "Bike",
          author: "danfador"
        }
      ]
    };


  }



  render() {
    const initialState = {
      theme: { primary: 'green' },
      cartItem: {
        items: 0,
        itemList: []
      }
    };

    function getCartItem(actionType) {
      console.log('Before after Updation', initialState.cartItem.items);
      if (actionType === 'Add') {

        initialState.cartItem.items++;

        return initialState.cartItem.items
      } else {
        initialState.cartItem.items--
        return initialState.cartItem.items
      }
    }
    function updateCartItem(item) {
      initialState.cartItem.itemList.push(item);
      return initialState.cartItem.itemList;

    }

    function removeCartItem(index) {
      initialState.cartItem.itemList.splice(index, 1);
      return initialState.cartItem.itemList;

    }
    const reducer = (state, action) => {
      switch (action.type) {
        case 'changeTheme':
          return {
            ...state,
            theme: action.newTheme
          };
        case 'updateCart':
          return {
            ...state,
            cartItem: {
              items: getCartItem('Add'),
              itemList: updateCartItem(action.cartItem)
            }
          }
        case 'removeCart':
          return {
            ...state,
            cartItem: {
              items: getCartItem('Substract'),
              itemList: removeCartItem(action.index)
            }
          }
        default:
          return state;
      }
    };

    // const mainReducer = (state, action) => {

    //   switch (action.type) {
    //     default:
    //       CartReducer(state, action)

    //   }
    // }
    return (
      <div className="App">
        <StateProvider initialState={initialState} reducer={reducer}>
          <Navigation navData={this.state.navigationData} />
          <main role="main">
            <Route />
          </main>


        </StateProvider>
      </div>
    );
  }
}

export default App;
